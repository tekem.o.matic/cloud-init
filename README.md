# cloud-init


# Samples

## Ansible pull
```
/usr/bin/ansible-pull --url=https://gitlab.com/tekem.o.matic/cloud-init.git ubuntu-test.yml
```

## Multipass:
```
cat cloud-init.yml | multipass launch -n my-test-vm --cloud-init -

PUB=$(cat ~/.ssh/id_rsa.pub ) sed -e "s|SSHPUB|$PUB|" cloud-init-basic-ansible.yml  | \
   multipass launch -n my-test-vm -m 2G -d 10G --cloud-init -

PUB=$(cat ~/.ssh/id_rsa.pub ) curl -s https://gitlab.com/tekem.o.matic/cloud-init/-/raw/main/cloud-init-samples/ci-parrotsec-ansible.yml | \
   sed -e "s|SSHPUB|$PUB|" | multipass launch -n my-test-vm -m 2G -d 10G --cloud-init -

```
## Testing Ansible
```
# ip of my-test-vm
multipass ls
Name                    State             IPv4             Image
my-test-vm              Running           10.199.217.86    Ubuntu 22.04 LTS

ansible-playbook -u ansible -i"10.199.217.86," sec-parrot.yml 

```


